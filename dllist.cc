#include "dllist.h"
#include "thread.h"
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
using namespace std;
extern int N;

DLLElement::DLLElement(void *itemPtr, int sortKey) {
	next = NULL;
	prev = NULL;
	key = sortKey;
	item = itemPtr;
}
DLList::DLList() {
	first = NULL;
	last = NULL;
}
DLList::~DLList() {
	if (!IsEmpty()) {
		while (first != last) {
			DLLElement* del = first;

			first = first->next;
			delete del;
		}
		delete first;
	}
}

void DLList::Prepend(void *item) {
	if (!IsEmpty()) {
		DLLElement* prepend = new DLLElement(item, (first->key - 1));

		prepend->next = first;
		first->prev = prepend;	
		first = prepend;
	}
	else {
		DLLElement* prepend = new DLLElement(item, 1);

		first = last = prepend;
	}
}
void DLList::Append(void *item) {
	if (!IsEmpty()) {
		DLLElement* append = new DLLElement(item, last->key + 1);

		last->next = append;
		append->prev = last;
		last = append;
	}
	else {
		DLLElement* append = new DLLElement(item, 1);
		first = last = append;
	}
}
void* DLList::Remove(int *keyPtr) {
	if (!IsEmpty()) {
	  *keyPtr = first->key;
	  return SortedRemove(*keyPtr);
	}
	else {
		return NULL;
	}
}
bool DLList::IsEmpty() {
	if (!first)
		return true;
	else
		return false;
}

void DLList::SortedInsert(void *item, int sortKey) {
	DLLElement * temp = new DLLElement(item, sortKey);
	DLLElement * ptr1, *ptr2;

	if (IsEmpty())
	{
		first = temp;
		last = temp;
	}
	else
	{
		for (ptr1 = first, ptr2 = NULL; ptr1 != NULL && ptr1->key < sortKey; ptr2 = ptr1, ptr1 = ptr1->next)
			;
			
			
	        if (ptr1 != NULL)
		{
			if (ptr2 != NULL)
			{
				temp->next = ptr1;
				temp->prev = ptr2;
				ptr2->next = temp;
				ptr1->prev = temp;
			}
			else
			{
				temp->next = first;
				first->prev = temp;
				first = temp;
			}
		}
		else
		{
			temp->prev = ptr2;
			ptr2->next = temp;
			last = temp;
		}
	}

}

void* DLList::SortedRemove(int sortKey) {
	if (IsEmpty()) {
		return NULL;
	}
	else {
		DLLElement *del;
		del = first;
		void* item;
		while (del != NULL)
		{
			if (del->key == sortKey)
			{
			       if (del == first && first->next) { 
					DLLElement *next = del->next;

					next->prev = NULL;
					del->next = NULL;
					first = next;
				}
				else if(del == first){
					first = last = NULL;
				}
				else if((del != first) && (del != last))
				{
					DLLElement * next = del->next;
					DLLElement * prev = del->prev;

					next->prev = prev;
					prev->next = next;
				}
				else
				{
					last = del->prev;
					last->next = NULL;
				}
				item = del->item;
				delete del;
				return item;
			}
			else
				del = del->next;
		}
	}

}
 
